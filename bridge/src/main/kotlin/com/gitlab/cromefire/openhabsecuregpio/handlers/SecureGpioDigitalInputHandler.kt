package com.gitlab.cromefire.openhabsecuregpio.handlers

import com.gitlab.cromefire.openhabsecuregpio.configuration.DigitalInputChannelConfiguration
import com.gitlab.cromefire.openhabsecuregpio.protocol.SecureGpioGrpcKt
import com.gitlab.cromefire.openhabsecuregpio.protocol.readPinConfiguration
import io.grpc.Status
import io.grpc.StatusException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.openhab.core.library.types.OnOffType
import org.openhab.core.types.Command
import org.openhab.core.types.State
import org.slf4j.Logger
import org.slf4j.LoggerFactory

internal class SecureGpioDigitalInputHandler(
    config: DigitalInputChannelConfiguration,
    client: SecureGpioGrpcKt.SecureGpioCoroutineStub,
    scope: CoroutineScope,
    updateState: (state: State) -> Unit
) : ChannelHandler<DigitalInputChannelConfiguration>(config, client, scope, updateState) {
    override val logger: Logger = LoggerFactory.getLogger(this::class.java)

    override fun validateParameters(config: DigitalInputChannelConfiguration) {
        if (config.gpioId < 0) {
            throw IllegalStateException("GPIO pin is unset")
        }
    }

    override fun reset() {}

    override fun startConnection(): Job {
        return scope.launch {
            try {
                client.readDigitalPin(readPinConfiguration { pin = config.gpioId }).collect {
                    updateState(OnOffType.from(it.value))
                }
            } catch (e: StatusException) {
                if (e.status.code == Status.UNAVAILABLE.code) {
                    logger.info("Pin ${config.gpioId} disconnected")
                    return@launch
                }
                logger.error("Unexpected status exception while sending message from channel", e)
            } catch (e: Throwable) {
                logger.error("Unknown exception while sending message from channel", e)
            }
        }
    }

    override suspend fun handleCommand(command: Command) {
        logger.debug("Input handlers don't handle commands")
    }
}
