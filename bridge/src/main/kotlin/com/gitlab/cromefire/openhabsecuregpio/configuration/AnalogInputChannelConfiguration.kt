package com.gitlab.cromefire.openhabsecuregpio.configuration

internal data class AnalogInputChannelConfiguration(
    var moduleId: Int? = null,
    var pin: Int? = null,
    var jitter: Int = 50
)
