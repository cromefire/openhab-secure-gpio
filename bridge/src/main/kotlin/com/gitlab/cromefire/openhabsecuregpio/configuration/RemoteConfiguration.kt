package com.gitlab.cromefire.openhabsecuregpio.configuration

internal class RemoteConfiguration {
    lateinit var host: String
    var port: Int = 0
    lateinit var serverCa: String
    lateinit var clientCert: String
    lateinit var clientKey: String
}
