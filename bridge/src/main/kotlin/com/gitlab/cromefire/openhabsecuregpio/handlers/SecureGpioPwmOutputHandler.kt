package com.gitlab.cromefire.openhabsecuregpio.handlers

import com.gitlab.cromefire.openhabsecuregpio.configuration.PwmOutputChannelConfiguration
import com.gitlab.cromefire.openhabsecuregpio.protocol.Gpio
import com.gitlab.cromefire.openhabsecuregpio.protocol.SecureGpioGrpcKt
import com.gitlab.cromefire.openhabsecuregpio.protocol.pwmPinSelect
import com.gitlab.cromefire.openhabsecuregpio.protocol.writePwmPin
import io.grpc.Status
import io.grpc.StatusException
import kotlin.math.roundToInt
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch
import org.openhab.core.library.types.DecimalType
import org.openhab.core.library.types.OnOffType
import org.openhab.core.library.types.PercentType
import org.openhab.core.types.Command
import org.openhab.core.types.State
import org.slf4j.Logger
import org.slf4j.LoggerFactory

internal class SecureGpioPwmOutputHandler(
    config: PwmOutputChannelConfiguration,
    client: SecureGpioGrpcKt.SecureGpioCoroutineStub,
    scope: CoroutineScope,
    updateState: (state: State) -> Unit
) : ChannelHandler<PwmOutputChannelConfiguration>(config, client, scope, updateState) {
    override val logger: Logger = LoggerFactory.getLogger(this::class.java)
    private var channel: Channel<Gpio.WritePwmPin> = Channel()

    override fun validateParameters(config: PwmOutputChannelConfiguration) {
        require(config.pinId >= 0) { "PWM pin is unset" }
        require(config.pwmModId >= 0) { "PWM module id pin is unset" }
    }

    override fun reset() {
        channel.close()
        channel = Channel()
    }

    override fun startConnection(): Job {
        val job = scope.launch {
            try {
                client.writePwmPin(channel.consumeAsFlow())
            } catch (e: StatusException) {
                if (e.status.code == Status.UNAVAILABLE.code) {
                    logger.info("Pin ${config.pinId} disconnected")
                    return@launch
                } else if (e.status.code == Status.CANCELLED.code) {
                    logger.info("Pin ${config.pinId} disconnected")
                    return@launch
                }
                logger.error("Unexpected status exception while sending message from channel", e)
            } catch (e: NullPointerException) {
                val c = e.cause
                if (c is StatusException && c.status.code == Status.CANCELLED.code) {
                    logger.info("Pin ${config.pinId} disconnected")
                    return@launch
                }
                throw e
            } catch (e: CancellationException) {
                throw e
            } catch (e: Throwable) {
                logger.error("Unknown exception while sending message from channel", e)
            }
        }
        scope.launch {
            try {
                logger.debug("Initializing pin ${config.pinId}")
                channel.send(writePwmPin {
                    pinSelect = pwmPinSelect {
                        pwmModule = config.pwmModId + 1
                        pin = config.pinId + 1
                    }
                })
            } catch (e: CancellationException) {
                throw e
            } catch (e: Throwable) {
                logger.error("Unknown exception while initializing channel", e)
            }
        }
        return job
    }

    override suspend fun handleCommand(command: Command) {
        when (command) {
            is PercentType -> {
                logger.debug("Setting pin ${config.pinId} to ${command.toFloat()}%")
                channel.send(writePwmPin {
                    value = (command.toDouble() * 2.55).roundToInt()
                })
                updateState(command)
            }
            is DecimalType -> {
                val pinVal = command.toDouble().roundToInt()
                logger.debug("Setting pin ${config.pinId} to $pinVal")
                channel.send(writePwmPin {
                    value = pinVal
                })
                updateState(command)
            }
            is OnOffType -> {
                logger.debug("Setting pin ${config.pinId} to $command")
                channel.send(writePwmPin {
                    value = when (command) {
                        OnOffType.ON -> 255
                        OnOffType.OFF -> 0
                    }
                })
                updateState(command)
            }
            else -> {
                logger.warn("Unknown command type ${command.javaClass}")
            }
        }
    }
}
