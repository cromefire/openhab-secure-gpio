package com.gitlab.cromefire.openhabsecuregpio.configuration

internal data class PwmOutputChannelConfiguration(var pwmModId: Int = -1, var pinId: Int = -1, var invert: Boolean = false)
