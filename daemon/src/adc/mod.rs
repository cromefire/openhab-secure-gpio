use async_trait::async_trait;
use std::error::Error;

pub mod mcp3000;

#[async_trait]
pub trait Adc {
    async fn listen_analog(
        &mut self,
        pin: u8,
        callback: Box<dyn FnMut(u16) + Send>,
    ) -> Result<(), Box<dyn Error>>;

    fn max_pin(&self) -> u8;
    fn max_value(&self) -> u16;
}
