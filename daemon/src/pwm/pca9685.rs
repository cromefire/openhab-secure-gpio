use crate::pwm::Pwm;
use async_trait::async_trait;
use rppal::i2c;
use rppal::i2c::I2c;
use std::error::Error;
use std::thread;
use std::time::Duration;
use thiserror::Error;

// REGISTER ADDRESSES
const PCA9685_MODE1: u8 = 0x00;
const PCA9685_LED0_ON_L: u8 = 0x06;
const PCA9685_PRESCALE: u8 = 0xfe;

// MODE1 bits
const MODE1_RESTART: u8 = 0x80;
const MODE1_SLEEP: u8 = 0x10;
const MODE1_AI: u8 = 0x20;

const PCA9685_I2C_ADDRESS: u16 = 0x40;
const FREQUENCY_OSCILLATOR: u32 = 25000000;

const PCA9685_PRESCALE_MIN: f32 = 3.;
const PCA9685_PRESCALE_MAX: f32 = 255.;

pub struct PCA9685Pwm {
    handler: PCA9685,
}

impl PCA9685Pwm {
    pub fn new(
        address: u8,
        freq: Option<f32>,
        oscillator_freq: Option<u32>,
    ) -> Result<PCA9685Pwm, PCA9685Error> {
        let mut handler = PCA9685::new(address)?;
        handler.begin(freq, oscillator_freq)?;

        Ok(PCA9685Pwm { handler })
    }
}

unsafe impl Send for PCA9685Pwm {}

unsafe impl Sync for PCA9685Pwm {}

#[async_trait]
impl Pwm for PCA9685Pwm {
    async fn set_pwm(&mut self, pin: u8, value: u16) -> Result<(), Box<dyn Error>> {
        self.handler.set_pwm(pin, 0, value)?;
        Ok(())
    }

    fn max_value(&self) -> u16 {
        4095
    }

    fn max_pin(&self) -> u8 {
        15
    }
}

#[derive(Error, Debug)]
pub enum PCA9685Error {
    #[error("address too large")]
    InvalidAddress,
    #[error("I²C error")]
    I2CError(#[from] i2c::Error),
}

struct PCA9685 {
    i2c: I2c,
    pub oscillator_freq: u32,
}

impl PCA9685 {
    pub fn new(address: u8) -> Result<PCA9685, PCA9685Error> {
        if address >= 62 {
            return Err(PCA9685Error::InvalidAddress);
        }
        let mut pwm = PCA9685 {
            i2c: I2c::new()?,
            oscillator_freq: 0,
        };
        pwm.i2c
            .set_slave_address(PCA9685_I2C_ADDRESS + address as u16)?;

        Ok(pwm)
    }

    pub fn begin(&mut self, freq: Option<f32>, oscillator_freq: Option<u32>) -> Result<(), PCA9685Error> {
        self.reset()?;

        self.set_pwm_freq(freq.unwrap_or(1000.))?;
        self.oscillator_freq = oscillator_freq.unwrap_or(FREQUENCY_OSCILLATOR);

        Ok(())
    }

    pub fn reset(&self) -> Result<(), PCA9685Error> {
        self.i2c.smbus_write_byte(PCA9685_MODE1, MODE1_RESTART)?;
        thread::sleep(Duration::from_millis(10));
        Ok(())
    }

    pub fn set_pwm_freq(&mut self, mut freq: f32) -> Result<(), PCA9685Error> {
        if freq < 1. {
            freq = 1.;
        }
        if freq > 3500. {
            freq = 3500.;
        }

        let mut prescaleval: f32 = ((self.oscillator_freq as f32 / (freq * 4096.0)) + 0.5) - 1.;
        if prescaleval < PCA9685_PRESCALE_MIN {
            prescaleval = PCA9685_PRESCALE_MIN;
        }
        if prescaleval > PCA9685_PRESCALE_MAX {
            prescaleval = PCA9685_PRESCALE_MAX;
        }
        let prescale: u8 = prescaleval as u8;
        let oldmode = self.i2c.smbus_read_byte(PCA9685_MODE1)?;
        let newmode = (oldmode & !MODE1_RESTART) | MODE1_SLEEP; // sleep
        self.i2c.smbus_write_byte(PCA9685_MODE1, newmode)?; // go to sleep
        self.i2c.smbus_write_byte(PCA9685_PRESCALE, prescale)?; // set the prescaler
        self.i2c.smbus_write_byte(PCA9685_MODE1, oldmode)?;
        thread::sleep(Duration::from_millis(5));
        self.i2c
            .smbus_write_byte(PCA9685_MODE1, oldmode | MODE1_RESTART | MODE1_AI)?;
        // This sets the MODE1 register to turn on auto increment.
        Ok(())
    }

    pub fn set_pwm(&mut self, num: u8, on: u16, off: u16) -> Result<(), PCA9685Error> {
        self.i2c.write(&[
            PCA9685_LED0_ON_L + 4 * num,
            on as u8,
            (on >> 8) as u8,
            off as u8,
            (off >> 8) as u8,
        ])?;
        Ok(())
    }
}
